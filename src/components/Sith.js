import React, { useState, useEffect } from 'react';

import { Redirect } from 'react-router-dom';

import Container from "muicss/lib/react/container";
import Form from "muicss/lib/react/form";
import Select from "muicss/lib/react/select";
import Col from 'muicss/lib/react/col';
import Button from "muicss/lib/react/button";
import Option from "muicss/lib/react/option";
import Row from "muicss/lib/react/row";
import Checkbox from "muicss/lib/react/checkbox";

import { BACK_URL } from "../config";


export const SithChoose = () => {

    const [siths, setSiths] = useState([]);
    const [choosenSith, setChoosenSith] = useState("");
    const [isRedirect, setRedirect] = useState(false);

    useEffect(() => {
        loadSiths();
    }, []);

    async function loadSiths() {
        const response = await fetch(BACK_URL + '/api/sith')
        const siths = await response.json();
        setSiths(siths)
    }

    function chooseSith(e) {
        e.preventDefault();
        localStorage.setItem("sith", choosenSith);
        setRedirect(true);
    }

    function redirect() {
        if (isRedirect) {
            return <Redirect to="/sith/recruit_results" />
        }
    }

    return (
        <Container>
            <Col md-offset="4" md="4">
                <Form onSubmit={chooseSith}>
                    <Select onChange={(e) => setChoosenSith(e.target.value)}>
                        <Option value="" label="Choose your name"/>
                        {siths.map((sith) => { return <Option value={sith.id} label={sith.name + " from " + sith.planet} key={sith.id} /> })}
                    </Select>
                    <Button variant="raised" type="submit">Select</Button>
                </Form>
            </Col>
            {redirect()}
        </Container>
    )
}


export const RecruitResults = () => {
    const [results, setResults] = useState([]);
    const [choosenRecruit, setChoosenRecruit] = useState([]);
    const [redirect, setRedirect] = useState(false);
    const [error, setError] = useState("")

    useEffect(() => {
        loadResults();
    }, []);

    async function loadResults() {
        const response = await fetch(BACK_URL + '/api/result');
        const results_ = await response.json();
        setResults(results_);
        console.log(results_);
    }

    function addRecruit(event) {
        setChoosenRecruit(choosenRecruit.concat(event.target.value));
    }

    const results_ = results.map((result) => {
        return (
            <Row key={result.recruit.id}>
                <Col md="1" md-offset="3"><Checkbox value={result.recruit_data.id} onChange={addRecruit} /></Col>
                <Col md="4">{result.recruit_data.name} From {result.recruit_data.planet}</Col>
                <Col md="3">{result.points}</Col>
            </Row>
        )
    });

    async function chooseRecruits(e) {
        e.preventDefault();
        console.log(choosenRecruit);
        for (var i in choosenRecruit) {
            let url = BACK_URL + '/api/sith/' + localStorage.getItem("sith") + '/recruit/' + choosenRecruit[i];
            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            });
            if(response.status === 400) {
                const error = await response.json();
                setError(error["error"]);
                return;
            }
        }
        setRedirect(true);
    }

    function isRedirect() {
        if (redirect) {
            return <Redirect to="/sith/done" />
        }
    }

    return (
        <Container>
            <Row className="header-sith">
                <Col md="2" md-offset="3">Choose recruit</Col>
                <Col md="3">Recruit</Col>
                <Col md="3">Points</Col>
            </Row>
            <Form onSubmit={chooseRecruits}>
                {results_}

                <Col md-offset="3">
                    {error}
                </Col>
                <Col md-offset="3">
                    <Button type="submit" variant="raised">Choose recruits</Button>
                </Col>
            </Form>
            {isRedirect()}
        </Container>
    )
}


export const SithsList = () => {

    const [siths, setSiths] = useState([]);

    useEffect(() => {
        loadSiths();
    }, []);

    async function loadSiths() {
        const response = await fetch(BACK_URL + '/api/sith/recruits');
        const siths_ = await response.json();
        setSiths(siths_);
    }

    const siths_ = siths.map(sith => {
        const recruits = sith.shadow_hands.map(recruit => {
            return recruit.name + ",   ";
        });
        return (
            <Row key={sith.id}>
                <Col md="2" md-offset="3">{sith.name}</Col>
                <Col md="3">{sith.planet}</Col>
                <Col md="3">{recruits}</Col>
            </Row>
        )
    });
    return (
        <Container>
            <Row className="header-sith">
                <Col md="2" md-offset="3">Sith name</Col>
                <Col md="3">Planet</Col>
                <Col md="3">recruits</Col>
            </Row>
            {siths_}
        </Container>
    )
}


export const SithDone = () => {
    const [redirect, setRedirect] = useState(false);

    useEffect(() => {
        waitRedirect();
    }, []);

    function waitRedirect() {
        const sr = setRedirect;
        setTimeout(() => {
            sr(true);
        }, 1500);
    }

    function isRedirect() {
        if(redirect) return <Redirect to="/sith/recruit_results" />
    }

    return (
        <Container>
            <h2>Recruits are accepted</h2>
            {isRedirect()}
        </Container>
    );
}