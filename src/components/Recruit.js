import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import Container from 'muicss/lib/react/container'
import Form from 'muicss/lib/react/form';
import Input from 'muicss/lib/react/input';
import Button from 'muicss/lib/react/button';
import Checkbox from "muicss/lib/react/checkbox";
import Row from "muicss/lib/react/row";
import Col from "muicss/lib/react/col";

import { BACK_URL } from "../config";

export const RecruitForm = (props) => {
    const [name, setName] = useState("");
    const [age, setAge] = useState(0);
    const [planet, setPlanet] = useState("");
    const [email, setEmail] = useState("");
    const [redirect, setRedirect] = useState(false);


    async function registerRecruit(e) {
        e.preventDefault();
        const userData = {
            name: name,
            age: age,
            planet: planet,
            email: email
        };
        const response = await fetch(BACK_URL + '/api/recruit', {
            method: 'POST',
            body: JSON.stringify(userData),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        });
        if (response.status === 201) {
            response.json().then(data => {
                localStorage.setItem("recruit", data["id"]);
            });
            setRedirect(true);
        }
    };

    function isRedirect() {
        if (redirect || localStorage.getItem("recruit")) return (<Redirect to="/recruit/task" />);
    }

    return (
        <Container>
            <Form onSubmit={registerRecruit}>
                <Input label="Full name" required={true} floatingLabel={true} autoFocus={true} onChange={(event) => setName(event.target.value)} />
                <Input label="Age" required={true} floatingLabel={true} type="number" onChange={(event) => setAge(event.target.value)} />
                <Input label="Planet" required={true} floatingLabel={true} onChange={(event) => setPlanet(event.target.value)} />
                <Input label="Email Address" type="email" floatingLabel={true} required={true} onChange={(event) => setEmail(event.target.value)} />
                <Button variant="raised" type="submit">Submit</Button>
            </Form>
            {isRedirect()}
        </Container>
    )
}

export const RecruitTestTask = () => {
    const [recruit, setRecruit] = useState(localStorage.getItem("recruit") || "");
    const [task, setTask] = useState({ tasks: [] });
    const [answers, setAnswers] = useState([]);
    const [redirect, setRedirect] = useState(false);

    useEffect(() => {
        loadTask();
        loadRecruit();
    }, []);

    async function loadRecruit() {
        setTimeout(() => {
            setRecruit(localStorage.getItem("recruit"));
        }, 300);
    }

    async function loadTask() {
        const response = await fetch(BACK_URL + '/api/task');
        const task_ = await response.json();
        setTask(task_);
        let answers_ = [];
        task_.tasks.forEach(t => {
            answers_.push({
                task: t.id,
                answer: false
            });
        });
        setAnswers(answers_);
    }

    function addAnswer(id, answer) {
        let newAnswers = JSON.parse(JSON.stringify(answers));
        for (var i in newAnswers) {
            if (newAnswers[i]['task'] === id) {
                newAnswers[i]['answer'] = answer;
            }
        }
        setAnswers(answers.concat({task: id, answer: answer}));
    }

    async function submitTask(e) {
        e.preventDefault();
        const data = {
            tasks: task.id,
            answers: answers,
            recruit: recruit
        }
        console.log(data);
        const response = await fetch(BACK_URL + '/api/result', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        });
        if (response.status === 201) {
            localStorage.setItem("task_solved", true);
            setRedirect(response.status === 201);
        }
    }

    function isRedirect() {
        if (redirect || localStorage.getItem("task_solved")) return (<Redirect to="/recruit/done" />);
    }

    const tasks = task.tasks.map(task_ => { return <Row key={task_.id} ><Col md="4">{task_.text}</Col><Col md="4"><Checkbox onChange={(e) => addAnswer(task_.id, e.target.checked)} /></Col></Row> });

    return (
        <Container>
            <Col md-offset="4">
                <Form onSubmit={submitTask}>
                    {tasks}
                    <Button variant="raised" type="submit">Submit</Button>
                </Form>
            </Col>
            {isRedirect()}
        </Container>
    )
}

export const RecruitDone = () => {
    return (
        <Container>
            <Col md-offset="4">
                <h2>Wait mail</h2>
            </Col>
        </Container>
    );
}