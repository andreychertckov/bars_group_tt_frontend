import React from 'react';
import { BrowserRouter, Route, Link, Switch } from 'react-router-dom';
import Container from 'muicss/lib/react/container'

import './App.css';
import { RecruitForm, RecruitTestTask, RecruitDone } from './components/Recruit';
import { SithChoose, RecruitResults, SithsList, SithDone } from './components/Sith';


function App() {
  return (
    <div className="App">
      <link href="//cdn.muicss.com/mui-0.9.43/css/mui.min.css" rel="stylesheet" type="text/css" media="screen" />
      <BrowserRouter>
        <Container className='select'>
          <Link to="/recruit/form">For recruits</Link>
          <Link to="/sith">For siths</Link>
          <Link to="/sith/list">List of siths</Link>
        </Container>
        <div id="main">
          <Switch>
            <Route path="/recruit/form" component={RecruitForm} />
            <Route path="/recruit/task" component={RecruitTestTask} />
            <Route path="/recruit/done" component={RecruitDone} />
            <Route path="/sith" exact component={SithChoose} />
            <Route path="/sith/recruit_results" component={RecruitResults} />
            <Route path="/sith/list" exact component={SithsList}/>
            <Route path="/sith/done" exact component={SithDone}/>
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
